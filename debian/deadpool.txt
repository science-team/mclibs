# This file lists all the files that were removed from the CERNLIB mclibs
# source for copyright reasons.  Given is the file or directory to delete
# relative to $CERN_ROOT/src/.

# --Kevin McCarty, 29 June 2006

# Authors of Pythia/Jetset did NOT give permission to release
# their code under GPL.  CERN should have checked this more carefully!!!
mclibs/jetset/
mclibs/pythia/

# Removal of Pythia/Jetset also necessitates removal of Fritiof, Lepto, Ariadne
mclibs/fritiof/
mclibs/ariadne/
mclibs/ariadne_407/
mclibs/lepto63/

